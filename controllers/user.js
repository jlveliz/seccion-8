const { response } = require("express");

const UsuarioGet = (req, res = response) => {

    const params  = req.query;

  res.json({
    msg: "get API - Controller",
    params
  });
};

const UsuarioPost = (req, res = response) => {
  const body = req.body;

  res.json({
    msg: "post API - Controller",
    body,
  });
};
const UsuarioPut = (req, res = response) => {
  const { id } = req.params;

  res.json({
    msg: "put API - Controller",
    id,
  });
};
const UsuarioDelete = (req, res = response) => {
  res.json({
    msg: "delete API - Controller",
  });
};

module.exports = {
  UsuarioGet,
  UsuarioDelete,
  UsuarioPost,
  UsuarioPut,
};
