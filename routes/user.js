const { Router } = require('express');
const { UsuarioGet, UsuarioPost, UsuarioPut, UsuarioDelete } = require('../controllers/user')

const router = Router();


router.get("/:id", UsuarioGet);

router.put("/:id", UsuarioPut);

router.post("/", UsuarioPost);

router.delete("/:id", UsuarioDelete);



module.exports = router;